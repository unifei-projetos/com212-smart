import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { WebservicesService } from 'src/app/webservices.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ramo-list',
  templateUrl: './ramo-list.component.html',
  styleUrls: ['./ramo-list.component.scss']
})
export class RamoListComponent implements OnInit {

  constructor(private fb: FormBuilder, private ws: WebservicesService) { }
  tipoPesquisa;
  dadosPesquisa;
  dadosExcluir;
  ramoAtividade;
  pesquisa;
  dadosFormulario;
  formulario: FormGroup;
  edit;
  flag = [];
  segmentos;

  ngOnInit(): void {
    this.criarFormulario();
    this.ws.getSegmento(null, null).subscribe((resposta: any) => {
      this.ramoAtividade = resposta;
    });
    this.ws.getSegmento(null,null).subscribe((resposta: any) => {
      this.segmentos = resposta;
    });
  }

  pesquisaTipo(tipo) {
    if (tipo != '') {
      this.tipoPesquisa = tipo;
    }
  }

  onPesquisa(dado) {
    this.dadosPesquisa = new FormGroup({
      pesquisa: new FormControl(dado, [Validators.required])
    });
    this.ws.getSegmento(this.tipoPesquisa, this.dadosPesquisa.value).subscribe((result: any) => {
      this.ramoAtividade = result;
    })
  }

  excluir(dado) {

    this.dadosExcluir = new FormGroup({
      nomeRamo: new FormControl(dado.nomeRamo, [Validators.required]),
      segmentoMercado: new FormControl(dado.segmentoMercado, [Validators.required]),
    });

    this.ws.getVerificaRamo(dado.nomeRamo).subscribe((result: any) => {
    

      if (result.length === 0) {
        this.ws.deleteRamoAtividade(this.dadosExcluir.value);
        if (result) {
          Swal.fire({
            title: 'Sucesso',
            text: 'Ramos de Atividade Excluido !',
            icon: 'success',
            showCancelButton: false,
          }).then((result) => {
            if (result.value) this.load()
          })
        }
      }else{
        Swal.fire({
          title: 'Atenção',
          text: 'Ramo de Atividade Vinculado a licitações !',
          icon: 'error',
          showCancelButton: false,
        })
      }

    });



  }


  editar(ramo) {
    this.dadosFormulario = ramo;
    
    this.edit = true;
  }

  load() {
    location.reload()
  }

  criarFormulario() {
    this.formulario = this.fb.group({
      nomeRamo: new FormControl('', [Validators.required]),
      segmentoMercado: new FormControl('', [Validators.required]),
      novoNomeRamo: new FormControl('', [Validators.required]),
      novoSegmentoMercado: new FormControl('', [Validators.required]),
    });

  }

  submitFormulario() {

    let verifica = false;
    this.formulario.value.nomeRamo = this.dadosFormulario.nomeRamo;
    this.formulario.value.segmentoMercado = this.dadosFormulario.segmentoMercado;
    if (this.formulario.value.novoNomeRamo === '') {
      this.flag[0] = true;
      verifica = true;
    }
    if (this.formulario.value.novoSegmentoMercado === '') {
      this.flag[1] = true;
      verifica = true;
    }
    
  
    if (!verifica) {

      let results = this.ws.putRamoAtividade(this.formulario.value);
      
      if (results) {
        Swal.fire({
          title: 'Sucesso',
          text: 'Na alteração dos dados!',
          icon: 'success',
          showCancelButton: false,
        }).then((result) => {
          if (result.value) this.load()
        })
      }
    } else {
      Swal.fire({
        title: 'Revise ',
        text: 'Dados informados !',
        icon: 'info',
        showCancelButton: false,
      })
    }


  }
}
