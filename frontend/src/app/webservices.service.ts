import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class WebservicesService {
  baseUrl = 'http://localhost:3000/';

  constructor(private httpClient: HttpClient) { }

  getEmpresa(tipo,dadoPesquisa) {
    let empresa: any = [];
    empresa = this.httpClient.put(`${this.baseUrl}list/empresa/${tipo}`,dadoPesquisa);
    return empresa;
  }

  getOrgao(tipo,dadoPesquisa) {
    return this.httpClient.put(`${this.baseUrl}list/orgao/${tipo}`,dadoPesquisa);
  }

  getPermissao(name){
    return this.httpClient.get(`${this.baseUrl}permissao/${name}`);
  }

  getRelatorio1(){
    return this.httpClient.get(`${this.baseUrl}list/relatorio1`);
  }

  getRelatorio2(cnpj){
    return this.httpClient.get(`${this.baseUrl}list/relatorio2/${cnpj}`);
  }
  getLicitacaoRegioes(name,dadosExtras){
    return this.httpClient.put(`${this.baseUrl}list/licitacao/${name}`,dadosExtras);
  }

  getListarHistorico(cnpj){
    return this.httpClient.get(`${this.baseUrl}list/hist_licitacao/${cnpj}`);
  }

  getSegmento(name,dados){
    return this.httpClient.put(`${this.baseUrl}list/ramo/${name}`,dados);
  }

  getCategoria() {
    return this.httpClient.get(`${this.baseUrl}list/categoria`)
  }

  getLicitacacoes(dadoPesquisa){
    return this.httpClient.put(`${this.baseUrl}list/licitacao`,dadoPesquisa);
  }

  getSubcategoria(categoria) {
    return this.httpClient.get(`${this.baseUrl}list/subcategoria/${categoria}`)
  }

  getVerificaRamo(tipo) {
    return this.httpClient.get(`${this.baseUrl}verifica/ramo/${tipo}`)
  }

  getLogin(dados){
    return this.httpClient.put(`${this.baseUrl}login`,dados);
  }

  getAtualizaLicitacoes(paginaNumero){
    return this.httpClient.get(`${this.baseUrl}atualiza/${paginaNumero}`);
    
  }

  postEmpresa(dadosEmpresa) {
    this.httpClient.post(`${this.baseUrl}add/empresa`, dadosEmpresa).toPromise();
    return 'Empresa cadastrado com sucesso';
  }

  postRamo(dados) {
    this.httpClient.post(`${this.baseUrl}add/ramo`, dados).toPromise();
    return true;
  }

  postHistorico(dados){
    this.httpClient.post(`${this.baseUrl}add/hist_licitacao`,dados).toPromise();
  }

  putEmpresa(cnpj, dadosEmpresa) {
    this.httpClient.put(`${this.baseUrl}edit/empresa/${cnpj}`, dadosEmpresa).toPromise();
    return true;
  }

  putOrgao(cnpj, dadosOrgao) {
    this.httpClient.put(`${this.baseUrl}edit/orgao/${cnpj}`, dadosOrgao).toPromise();
    return true;
  }

  putRamoAtividade(dados) {
    this.httpClient.put(`${this.baseUrl}edit/ramo`, dados).toPromise();
    return true;
  }

  editLicitacao(nome,dados) {
    this.httpClient.put(`${this.baseUrl}edit/licitacao/${nome}`, dados).toPromise();
    return true;
  }
  

  deleteEmpresa(cnpj) {
    this.httpClient.delete(`${this.baseUrl}delete/empresa/${cnpj}`).toPromise();
    return true;
  }

  deleteLicitacao(nome) {
    this.httpClient.delete(`${this.baseUrl}delete/licitacao/${nome}`).toPromise();
    return true;
  }

  deleteOrgao(cnpj) {
    this.httpClient.delete(`${this.baseUrl}delete/orgao/${cnpj}`).toPromise();
    return true;
  }

  postOrgao(dadosOrgao) {
    this.httpClient.post(`${this.baseUrl}add/orgao`, dadosOrgao).toPromise();
    return 'Orgao cadastrado com sucesso';
  }

  deleteRamoAtividade(dados) {
    this.httpClient.put(`${this.baseUrl}delete/ramo`,dados).toPromise();
    return true;
  }

  //   getCategorias() {
  //     return this.httpClient.get(`${this.baseUrl}categorias`);
  //   }

  //   getUmaCategoria(CategoryID) {
  //     return this.httpClient.get(`${this.baseUrl}categoria/${CategoryID}`);
  //   }

  //   getPesqLivros(texto) {
  //     return this.httpClient.get(`${this.baseUrl}pesquisa/${texto}`);
  //   }

  //   getDadosConfirmacaoEndereco(email) {
  //     console.log(email);
  //     return this.httpClient.get(`${this.baseUrl}confirma/endereco/${email}`);
  //   }

  //   updateEndereco(id, dados){
  //     this.httpClient.put(`${this.baseUrl}confirma/endereco/atualiza/${id}`, dados).toPromise();
  //     return 'Update realizado com sucesso'
  //   }

  //   addLivro(umLivro) {
  //     this.httpClient.post(`${this.baseUrl}add/livro`, umLivro).toPromise();
  //     return 'Livro adicionado com sucesso';
  //   }
  //   // -----------------------------------
  //   addOrdem(custID) {

  //     this.httpClient.post(`${this.baseUrl}order`, custID).toPromise();
  //     return 'Ordem cadastradas';
  //   }

  //   getOrderID() {
  //     return this.httpClient.get(`${this.baseUrl}retorno`);
  //   }

  //   getIdCliente(email) {
  //     return this.httpClient.get(`${this.baseUrl}${email}`);
  //   }

  //   addItem(item) {
  //     console.log(item)
  //     this.httpClient.post(`${this.baseUrl}add/item`, item).toPromise();
  //     return 'Sucesso item adicionado';
  //   }



}