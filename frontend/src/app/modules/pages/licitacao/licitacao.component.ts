import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import { FormControl, Validators, FormGroup, FormBuilder } from '@angular/forms';
import { WebservicesService } from 'src/app/webservices.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-licitacao',
  templateUrl: './licitacao.component.html',
  styleUrls: ['./licitacao.component.scss']
})
export class LicitacaoComponent implements OnInit {

  areaInteresse;
  permissao = false;
  dadosExtra = new FormGroup({
    areaInteresse: new FormControl('', [Validators.required]),
    categoriaAtividade: new FormControl('', [Validators.required]),
    nomeEmpresa: new FormControl('', [Validators.required]),
  });
  cnpjEmpresa;
  constructor(private fb: FormBuilder, private ws: WebservicesService,private router : Router) { 
    const  name = router.url.split('/')[2];
    ws.getPermissao(name).subscribe((result: any)=>{
      if(result[0].nomeEmpresa === 'TIprime'){
        this.permissao = true;
        this.dadosExtra.value.categoriaAtividade = null;
      }else{
        this.dadosExtra.value.categoriaAtividade = result[0].categoriaAtividade;
      }
      this.cnpjEmpresa = result[0].cnpjEmpresa;
      this.areaInteresse = result[0].areaInteresse;
      this.dadosExtra.value.areaInteresse = result[0].areaInteresse;
      this.dadosExtra.value.nomeEmpresa = result[0].nomeEmpresa;
      this.ws.getLicitacaoRegioes(this.areaInteresse,this.dadosExtra.value).subscribe((resposta: any) => {
        this.licitacoes = resposta;
      });
    })
  }


  formulario: FormGroup;
  formularioHistorico: FormGroup;
  licitacoes: any;
  edit: Boolean = false;
  flag = [];
  dadosFormulario;
  pesquisa = '';
  tipoPesquisa;
  cont = 0;
  dadosPesquisa: FormGroup;
  categorias;
  situacoes = ['','Aberto', 'Cancelado','Suspenso','Participando'];

  ngOnInit(): void {
    this.dadosFormulario = '';
    this.criarFormulario();
    this.ws.getCategoria().subscribe((resposta: any) => {
      this.categorias = resposta;
    });
   
  }

  onPesquisa(dado) {

    this.dadosPesquisa = new FormGroup({
      nome: new FormControl('', [Validators.required]),
      datainicial: new FormControl('', [Validators.required]),
      datafinal: new FormControl('', [Validators.required]),
      areaInteresse:new FormControl('', [Validators.required]),
    });

    switch (this.tipoPesquisa) {
      case "nomeLicitacao":
        this.dadosPesquisa.value.nome = dado;
        this.dadosPesquisa.value.areaInteresse = this.areaInteresse;
        break;
      case "dataInicial":
        this.dadosPesquisa.value.datainicial = dado;
        this.dadosPesquisa.value.areaInteresse = this.areaInteresse;
        break;
      case "dataFinal":
        this.dadosPesquisa.value.datafinal = dado;
        this.dadosPesquisa.value.areaInteresse = this.areaInteresse;
        break;
    }

    this.ws.getLicitacacoes(this.dadosPesquisa.value).subscribe((result: any) => {
      this.licitacoes = result;
    })
  }

  onAtualiza() {
    this.cont++; 
    
    this.ws.getAtualizaLicitacoes(this.cont).subscribe((result: any)=>{
      if(result){
        Swal.fire({
          title: 'Sucesso',
          text: 'Carregamento de Licitação!',
          icon: 'success',
          showCancelButton: false,
        }).then((result) => {
          
        })
      }
    })
  }

  pesquisaTipo(tipo) {
    if (tipo != '') {
      this.tipoPesquisa = tipo;
    }
  }

  excluir(licitacao) {

    let result = this.ws.deleteLicitacao(licitacao.nomeLicitacao);
    if (result) {
      Swal.fire({
        title: 'Sucesso',
        text: 'Licitação removida do sistema !',
        icon: 'success',
        showCancelButton: false,
      }).then((result) => {
        if (result.value) this.load()
      })
    }
  }

  editar(empresa) {
    this.dadosFormulario = empresa;
    this.edit = true;
    this.formulario.value.nome = empresa.nomeLicitacao;
  }


  criarFormulario() {

    this.formulario = this.fb.group({
      situacao: new FormControl('', [Validators.required]),
      observacao: new FormControl('', [Validators.required]),
      segMercado: new FormControl('', [Validators.required]),
      valor: new FormControl('', [Validators.required]),
      nome: new FormControl('', [Validators.required])
    });

  }

  submitFormulario() {

    let verifica = false;
    if (this.formulario.value.situacao === '') {
      this.flag[0] = true;
      verifica = true;
    }
    if (this.formulario.value.observacao === '') {
      this.flag[1] = true;
      verifica = true;
    }
    
    if(this.formulario.value.situacao === 'Participando'){
      this.atualizarHistorioLicitacao(this.dadosFormulario.idLicitacao,this.cnpjEmpresa);
    }
    this.formulario.value.nome = this.dadosFormulario.nomeLicitacao;
    if (!verifica) {
      let results = this.ws.editLicitacao(this.dadosFormulario.nomeLicitacao, this.formulario.value);
      if (results) {
        Swal.fire({
          title: 'Sucesso',
          text: 'Na alteração dos dados!',
          icon: 'success',
          showCancelButton: false,
        }).then((result) => {
          if (result.value) this.load()
        })
      }
    } else {
      Swal.fire({
        title: 'Revise ',
        text: 'Dados informados !',
        icon: 'info',
        showCancelButton: false,
      })
    }

  }

  atualizarHistorioLicitacao(idLicitacao, idEmpresa){
    this.formularioHistorico = this.fb.group({
      idLicitacao: new FormControl(idLicitacao, [Validators.required]),
      cnpjEmpresa: new FormControl(idEmpresa, [Validators.required])
    });

    this.ws.postHistorico(this.formularioHistorico.value);
  }

  listarHistorico(){
    this.ws.getListarHistorico(this.cnpjEmpresa).subscribe((result: any)=>{
      if(result.length > 0){
        this.licitacoes = result;
      }else{
        Swal.fire({
          title: 'Aviso',
          text: 'Empresa ainda não está participando de nenhuma licitação, participe !',
          icon: 'info',
          showCancelButton: false,
        })
      }

    })
  }

  load() {
    location.reload()
  }
}
