import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PagesComponent} from './pages/pages.component';
import {DashBoardComponent} from './dash-board/dash-board.component';
import { EmpresaCreateComponent } from './empresa/empresa-create/empresa-create.component';
import { EmpresaListComponent } from './empresa/empresa-list/empresa-list.component';
import { OrgaoCreateComponent } from './orgao/orgao-create/orgao-create.component';
import { OrgaoListComponent } from './orgao/orgao-list/orgao-list.component';
import { LicitacaoComponent } from './licitacao/licitacao.component';
import { RamoCreateComponent } from './ramoAtividade/ramo-create/ramo-create.component';
import { RamoListComponent } from './ramoAtividade/ramo-list/ramo-list.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';


const routes: Routes = [
  {
    path: '', component: PagesComponent, children: [
      {path: '1', component: DashBoardComponent},
      {path: 'empresaCreate', component: EmpresaCreateComponent},
      {path: 'empresaList', component: EmpresaListComponent},
      {path: 'orgaoCreate', component: OrgaoCreateComponent},
      {path: 'orgaoList', component: OrgaoListComponent},
      {path: 'licitacaoList', component: LicitacaoComponent},
      {path: 'ramoCreate', component: RamoCreateComponent},
      {path: 'ramoList', component: RamoListComponent},
      {path: 'relatorios', component: RelatoriosComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PagesRoutingModule {
}
