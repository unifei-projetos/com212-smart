import { Component, OnInit } from '@angular/core';
import { WebservicesService } from 'src/app/webservices.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-orgao-list',
  templateUrl: './orgao-list.component.html',
  styleUrls: ['./orgao-list.component.scss']
})
export class OrgaoListComponent implements OnInit {

  constructor(private fb: FormBuilder, private ws: WebservicesService) { }


  formulario: FormGroup;
  orgaos: any;
  edit: Boolean = false;
  flag = [];
  dadosFormulario;
  pesquisa = '';
  tipoPesquisa;
  dadosPesquisa: FormGroup;
  estados = [
    { sigla: 'AC', estado: 'Acre' },
    { sigla: 'AL', estado: 'Alagoas' },
    { sigla: 'AP', estado: 'Amapá' },
    { sigla: 'AM', estado: 'Amazonas' },
    { sigla: 'BA', estado: 'Bahia' },
    { sigla: 'CE', estado: 'Ceará' },
    { sigla: 'DF', estado: 'Distrito Federal' },
    { sigla: 'EP', estado: 'Espírito Santo' },
    { sigla: 'GO', estado: 'Goiás' },
    { sigla: 'MA', estado: 'Maranhão' },
    { sigla: 'MT', estado: 'Mato Grosso' },
    { sigla: 'MS', estado: 'Mata Grosso do Sul' },
    { sigla: 'MG', estado: 'Minas Gerais' },
    { sigla: 'PA', estado: 'Pará' },
    { sigla: 'PB', estado: 'Paraíba' },
    { sigla: 'PR', estado: 'Pernambuco' },
    { sigla: 'PI', estado: 'Piauí' },
    { sigla: 'RJ', estado: 'Rio de Janeiro' },
    { sigla: 'RN', estado: 'Rio Grande do Norte' },
    { sigla: 'RS', estado: 'Rio Grande do Sul' },
    { sigla: 'RO', estado: 'Rondônia' },
    { sigla: 'RR', estado: 'Roraima' },
    { sigla: 'SC', estado: 'Santa Catarina' },
    { sigla: 'SP', estado: 'São Paulo' },
    { sigla: 'SE', estado: 'Sergipe' },
    { sigla: 'TO', estado: 'Tocantins' },
  ];


  ngOnInit(): void {
    this.dadosFormulario = '';
    this.criarFormulario();
    this.ws.getOrgao('', this.dadosPesquisa).subscribe((resposta: any) => {
      this.orgaos = resposta;
    });
  }

  onPesquisa(dado) {

    this.dadosPesquisa = new FormGroup({
      pesquisa: new FormControl(dado, [Validators.required])
    });
    this.ws.getOrgao(this.tipoPesquisa, this.dadosPesquisa.value).subscribe((result: any) => {
      this.orgaos = result;
    })
  }

  pesquisaTipo(tipo) {
    if (tipo != '') {
      this.tipoPesquisa = tipo;
    }
  }

  excluir(orgao) {
    let result = this.ws.deleteOrgao(orgao.cnpj);
    if (result) {
      Swal.fire({
        title: 'Sucesso',
        text: 'Orgão desativada do sistema !',
        icon: 'success',
        showCancelButton: false,
      }).then((result) => {
        if (result.value) this.load()
      })
    }
  }

  editar(empresa) {
    this.dadosFormulario = empresa;
    this.edit = true;
  }


  criarFormulario() {

    this.formulario = this.fb.group({
      cnpjOrgao: new FormControl('', [Validators.required]),
      nomeOrgao: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      telefone: new FormControl('', [Validators.required]),
      cep: new FormControl('', [Validators.required]),
      estado: new FormControl('', [Validators.required]),
      cidade: new FormControl('', [Validators.required]),
      endereco: new FormControl('', [Validators.required])
    });

  }

  submitFormulario() {

    let verifica = false;
    if (this.formulario.value.nomeOrgao === '') {
      this.flag[0] = true;
      verifica = true;
    }
    if (this.formulario.value.cnpjOrgao === '') {
      this.flag[1] = true;
      verifica = true;
    }
    if (this.formulario.value.telefone === '') {
      this.flag[2] = true;
      verifica = true;
    }
    if (this.formulario.value.email === '') {
      this.flag[3] = true;
      verifica = true;
    }
    if (this.formulario.value.cep === '') {
      this.flag[4] = true;
      verifica = true;
    }
    if (this.formulario.value.estado === '') {
      this.flag[5] = true;
      verifica = true;
    }
    if (this.formulario.value.cidade === '') {
      this.flag[6] = true;
      verifica = true;
    }
    if (this.formulario.value.endereco === '') {
      this.flag[7] = true;
      verifica = true;
    }

    if (!verifica) {
      let results = this.ws.putOrgao(this.formulario.value.cnpjOrgao, this.formulario.value);
      if (results) {
        Swal.fire({
          title: 'Sucesso',
          text: 'Na alteração dos dados!',
          icon: 'success',
          showCancelButton: false,
        }).then((result) => {
          if (result.value) this.load()
        })
      }
    } else {
      Swal.fire({
        title: 'Revise ',
        text: 'Dados informados !',
        icon: 'info',
        showCancelButton: false,
      })
    }
  }

  load() {
    location.reload()
  }

}
