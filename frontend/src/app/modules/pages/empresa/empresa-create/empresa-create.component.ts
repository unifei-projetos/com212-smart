import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { WebservicesService } from 'src/app/webservices.service';
import Swal from 'sweetalert2'

@Component({
  selector: 'app-empresa-create',
  templateUrl: './empresa-create.component.html',
  styleUrls: ['./empresa-create.component.scss']
})
export class EmpresaCreateComponent implements OnInit {

  constructor(private fb: FormBuilder, private ws: WebservicesService) {

  }
  flag = [];
  interesse = '';
  formulario: FormGroup;
  todosInteresse = [];
  areaInteresse: any = [
    { valor: 'SP', name: 'São Paulo' },
    { valor: 'MG', name: 'Minas Gerais' },
    { valor: 'RJ', name: 'Rio de Janeiro' }
  ];
  categorias;
  subCategorias;

  ngOnInit(): void {
    this.criarFormulario();
    this.ws.getCategoria().subscribe((resposta: any) => {
      this.categorias = resposta;
    });
  }

  onCheckboxChange(e, valor) {
    
    if (this.formulario.value.typeCheckBox) {
      this.todosInteresse.push(valor)
    } else {
      let index = this.todosInteresse.indexOf(valor);
      this.todosInteresse.splice(index, 1);
    }
  }

  onChangeEscolha(categoria) {
    this.ws.getSubcategoria(categoria).subscribe((resposta: any) => {
      this.subCategorias = resposta;
    });
  }

  submitFormulario() {
    this.todosInteresse.forEach(e => {
      this.interesse += e + ',';
    })
    let verifica = false;
    if (this.formulario.value.nomeEmpresa === '') {
      this.flag[0] = true;
      verifica = true;
    }
    if (this.formulario.value.nomeContato === '') {
      this.flag[1] = true;
      verifica = true;
    }
    if (this.formulario.value.senhaAcesso === '') {
      this.flag[2] = true;
      verifica = true;
    }
    if (this.formulario.value.telefone === '') {
      this.flag[3] = true;
      verifica = true;
    }
    if (this.formulario.value.cnpjEmpresa === '') {
      this.flag[4] = true;
      verifica = true;
    }
    if (this.formulario.value.email === '') {
      this.flag[5] = true;
      verifica = true;
    }
    if (this.formulario.value.endereco === '') {
      this.flag[6] = true;
      verifica = true;
    }
    if (this.formulario.value.categoriaAtividade === '') {
      this.flag[7] = true;
      verifica = true;
    }
    if (this.formulario.value.subcategoriaAtividade === '') {
      this.flag[8] = true;
      verifica = true;
    }
    if (this.formulario.value.areaInteresse === '') {
      this.flag[9] = true;
      verifica = true;
    } else {
      this.formulario.value.areaInteresse = this.interesse.substring(0,(this.interesse.length - 1));
      
    }
    if (this.formulario.value.tipoPlano === '') {
      this.flag[10] = true;
      verifica = true;
    }

    if (this.formulario.value.areaInteresse != '') {
      
      let results = this.ws.postEmpresa(this.formulario.value);
      if (results === 'Empresa cadastrado com sucesso') {
        Swal.fire({
          title: 'Sucesso',
          text: 'Na criação de uma empresa!',
          icon: 'success',
          showCancelButton: false,
        }).then((result) => {
          if (result.value) this.load()
        })
      }
    } else {
      Swal.fire({
        title: 'Revise ',
        text: 'Dados informados !',
        icon: 'info',
        showCancelButton: false,
      })
    }


  }

  criarFormulario() {
    this.formulario = this.fb.group({
      cnpjEmpresa: new FormControl('', [Validators.required]),
      senhaAcesso: new FormControl('', [Validators.required]),
      nomeContato: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      telefone: new FormControl('', [Validators.required]),
      nomeEmpresa: new FormControl('', [Validators.required]),
      endereco: new FormControl('', [Validators.required]),
      categoriaAtividade: new FormControl('', [Validators.required]),
      subcategoriaAtividade: new FormControl('', [Validators.required]),
      areaInteresse: new FormControl(this.todosInteresse, [Validators.required]),
      tipoPlano: new FormControl('', [Validators.required]),
      typeCheckBox: new FormControl(null,[Validators.required])
    });
  }

  load() {
    location.reload()
  }
}
