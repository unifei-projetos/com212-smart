import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2'
import { WebservicesService } from 'src/app/webservices.service';

@Component({
  selector: 'app-orgao-create',
  templateUrl: './orgao-create.component.html',
  styleUrls: ['./orgao-create.component.scss']
})
export class OrgaoCreateComponent implements OnInit {

  constructor(private fb: FormBuilder, private ws: WebservicesService) {

  }
  flag = [];
  formulario: FormGroup;
  estados = [
    { sigla: 'AC', estado: 'Acre' },
    { sigla: 'AL', estado: 'Alagoas' },
    { sigla: 'AP', estado: 'Amapá' },
    { sigla: 'AM', estado: 'Amazonas' },
    { sigla: 'BA', estado: 'Bahia' },
    { sigla: 'CE', estado: 'Ceará' },
    { sigla: 'DF', estado: 'Distrito Federal' },
    { sigla: 'EP', estado: 'Espírito Santo' },
    { sigla: 'GO', estado: 'Goiás' },
    { sigla: 'MA', estado: 'Maranhão' },
    { sigla: 'MT', estado: 'Mato Grosso' },
    { sigla: 'MS', estado: 'Mata Grosso do Sul' },
    { sigla: 'MG', estado: 'Minas Gerais' },
    { sigla: 'PA', estado: 'Pará' },
    { sigla: 'PB', estado: 'Paraíba' },
    { sigla: 'PR', estado: 'Pernambuco' },
    { sigla: 'PI', estado: 'Piauí' },
    { sigla: 'RJ', estado: 'Rio de Janeiro' },
    { sigla: 'RN', estado: 'Rio Grande do Norte' },
    { sigla: 'RS', estado: 'Rio Grande do Sul' },
    { sigla: 'RO', estado: 'Rondônia' },
    { sigla: 'RR', estado: 'Roraima' },
    { sigla: 'SC', estado: 'Santa Catarina' },
    { sigla: 'SP', estado: 'São Paulo' },
    { sigla: 'SE', estado: 'Sergipe' },
    { sigla: 'TO', estado: 'Tocantins' },
  ];

  ngOnInit(): void {
    this.criarFormulario();
  }

  submitFormulario() {
    let verifica = false;
    if (this.formulario.value.nomeOrgao === '') {
      this.flag[0] = true;
      verifica = true;
    }
    if (this.formulario.value.cnpjOrgao === '') {
      this.flag[1] = true;
      verifica = true;
    }
    if (this.formulario.value.telefone === '') {
      this.flag[2] = true;
      verifica = true;
    }
    if (this.formulario.value.email === '') {
      this.flag[3] = true;
      verifica = true;
    }
    if (this.formulario.value.cep === '') {
      this.flag[4] = true;
      verifica = true;
    }
    if (this.formulario.value.estado === '') {
      this.flag[5] = true;
      verifica = true;
    }
    if (this.formulario.value.cidade === '') {
      this.flag[6] = true;
      verifica = true;
    }
    if (this.formulario.value.endereco === '') {
      this.flag[7] = true;
      verifica = true;
    }


    if (!verifica) {
      let results = this.ws.postOrgao(this.formulario.value);
      if (results === 'Orgao cadastrado com sucesso') {
        Swal.fire({
          title: 'Sucesso',
          text: 'Na criação de um Orgão!',
          icon: 'success',
          showCancelButton: false,
        }).then((result) => {
          if (result.value) this.load();
        })
      }
    } else {
      Swal.fire({
        title: 'Revise ',
        text: 'Dados informados !',
        icon: 'info',
        showCancelButton: false,
      })
    }


  }

  criarFormulario() {
    this.formulario = this.fb.group({
      cnpjOrgao: new FormControl('', [Validators.required]),
      nomeOrgao: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      telefone: new FormControl('', [Validators.required]),
      cep: new FormControl('', [Validators.required]),
      estado: new FormControl('', [Validators.required]),
      cidade: new FormControl('', [Validators.required]),
      endereco: new FormControl('', [Validators.required])
    });
  }

  load() {
    location.reload()
  }

}
