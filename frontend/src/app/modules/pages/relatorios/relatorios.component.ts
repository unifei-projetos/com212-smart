import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { WebservicesService } from 'src/app/webservices.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-relatorios',
  templateUrl: './relatorios.component.html',
  styleUrls: ['./relatorios.component.scss']
})
export class RelatoriosComponent implements OnInit {

  cnpjEmpresa;
  permissao = false;
  constructor(private ws: WebservicesService, private router: Router) {
    const name = router.url.split('/')[2];
    ws.getPermissao(name).subscribe((result: any) => {
      this.cnpjEmpresa = result[0].cnpjEmpresa;
      if(result[0].nomeEmpresa === 'TIprime'){
        this.permissao = true;
      }
    })

  }
  flag = false;
  url = '';
  ngOnInit(): void {
  }

  gerarRelatorio() {
    this.ws.getRelatorio1().subscribe((res: any) => {
     
      // this.url = "file:"+res.filename;
      // console.log(this.url);
      // window.open(this.url, '_blank');
      // this.flag =true;
      //  var file = new Blob([res.filename], {type: 'application/pdf'});
      var myBlob = new Blob([res], { type: 'text/html' })
      var url = URL.createObjectURL(myBlob);
      window.open(url);
    });
  }

  gerarRelatorio2() {
    this.ws.getRelatorio2(this.cnpjEmpresa).subscribe((res: any) => {
      var myBlob = new Blob([res], { type: 'text/html' })
      var url = URL.createObjectURL(myBlob);
      window.open(url);
    })

  }

}
