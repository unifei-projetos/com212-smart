import {Component, OnDestroy, OnInit} from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { WebservicesService } from 'src/app/webservices.service';
import Swal from 'sweetalert2';
import { Router } from '@angular/router';

declare var $;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, OnDestroy {

  constructor(private fb: FormBuilder, private ws: WebservicesService, private router: Router) { }
  
  formulario: FormGroup;
  email = '';
  senha = '';

  ngOnInit() {
    this.criarFormulario();
    $('body').addClass('hold-transition login-page');
    $(() => {
      $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' /* optional */
      });
    });
  }

  ngOnDestroy(): void {
    $('body').removeClass('hold-transition login-page');
  }

  onLogin(){
  this.ws.getLogin(this.formulario.value).subscribe((resposta: any) => {

    if(resposta.length){
      this.router.navigate([`members/${resposta[0].cnpjEmpresa}`]);
    }else{
      Swal.fire({
        title: 'Erro',
        text: 'Usuario invalido !',
        icon: 'error',
        showCancelButton: false,
      }).then((result) => {
        if (result.value) this.load()
      })
    }
  });

    
  }

  criarFormulario() {
    this.formulario = this.fb.group({
      email: new FormControl('', [Validators.required]),
      senha: new FormControl('', [Validators.required]),
    });

  }

  load() {
    location.reload()
  }
}
