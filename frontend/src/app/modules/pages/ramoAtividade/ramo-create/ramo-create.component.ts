import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { WebservicesService } from 'src/app/webservices.service';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-ramo-create',
  templateUrl: './ramo-create.component.html',
  styleUrls: ['./ramo-create.component.scss']
})
export class RamoCreateComponent implements OnInit {

  constructor(private fb: FormBuilder, private ws: WebservicesService) { }
  formulario: FormGroup;
  flag = [];
  segmentos;

  ngOnInit(): void {
    this.criarFormulario();
    this.ws.getSegmento(null,null).subscribe((resposta: any) => {
      this.segmentos = resposta;
    });
  }

  submitFormulario() {
    let verifica = false;
    if (this.formulario.value.nomeRamo === '') {
      this.flag[0] = true;
      verifica = true;
    }
    if (this.formulario.value.segmentoMercado === '') {
      this.flag[1] = true;
      verifica = true;
    }
    

    if (!verifica) {
      let results = this.ws.postRamo(this.formulario.value);
      if (results) {
        Swal.fire({
          title: 'Sucesso',
          text: 'Na criação de uma Ramo de atividade!',
          icon: 'success',
          showCancelButton: false,
        }).then((result) => {
          if (result.value) this.load();
        })
      }
    } else {
      Swal.fire({
        title: 'Revise ',
        text: 'Dados informados !',
        icon: 'info',
        showCancelButton: false,
      })
    }


  }

  criarFormulario() {
    this.formulario = this.fb.group({
      nomeRamo: new FormControl('', [Validators.required]),
      segmentoMercado: new FormControl('', [Validators.required])
    });
  }

  load() {
    location.reload()
  }

}
