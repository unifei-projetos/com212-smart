import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgaoListComponent } from './orgao-list.component';

describe('OrgaoListComponent', () => {
  let component: OrgaoListComponent;
  let fixture: ComponentFixture<OrgaoListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgaoListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgaoListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
