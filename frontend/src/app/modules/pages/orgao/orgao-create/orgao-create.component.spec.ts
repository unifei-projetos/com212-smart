import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgaoCreateComponent } from './orgao-create.component';

describe('OrgaoCreateComponent', () => {
  let component: OrgaoCreateComponent;
  let fixture: ComponentFixture<OrgaoCreateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgaoCreateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgaoCreateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
