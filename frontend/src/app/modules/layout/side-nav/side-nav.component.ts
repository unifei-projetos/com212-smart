import {Component, OnInit} from '@angular/core';
import { WebservicesService } from 'src/app/webservices.service';
import { Router } from '@angular/router';

declare var $;

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss']
})
export class SideNavComponent implements OnInit {

  permissao = false;
  nomeEmpresa;
  flag = false;
  nome;
  constructor(private ws: WebservicesService,private router : Router) {
    const  name = router.url.split('/')[2];
    ws.getPermissao(name).subscribe((result: any)=>{
      
      if(result[0].permissao === 1){
        this.permissao = true;
      }
      this.nomeEmpresa = result[0].nomeEmpresa;
      if(this.nomeEmpresa === 'TIprime'){
        this.flag = true;
      }
    })
  }

  ngOnInit() {


    $(document).ready(() => {
      $('.sidebar-menu').tree();
    });
  }

}
