import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {PagesRoutingModule} from './pages-routing.module';
import {PagesComponent} from './pages/pages.component';
import {DashBoardComponent} from './dash-board/dash-board.component';
import {LayoutModule} from '../layout/layout.module';
import { EmpresaCreateComponent } from './empresa/empresa-create/empresa-create.component';
import { EmpresaListComponent } from './empresa/empresa-list/empresa-list.component';
import { OrgaoCreateComponent } from './orgao/orgao-create/orgao-create.component';
import { OrgaoListComponent } from './orgao/orgao-list/orgao-list.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LicitacaoComponent } from './licitacao/licitacao.component';
import { RamoCreateComponent } from './ramoAtividade/ramo-create/ramo-create.component';
import { RamoListComponent } from './ramoAtividade/ramo-list/ramo-list.component';
import { RelatoriosComponent } from './relatorios/relatorios.component';



@NgModule({
  declarations: [PagesComponent, DashBoardComponent, EmpresaCreateComponent, EmpresaListComponent, OrgaoCreateComponent, OrgaoListComponent, LicitacaoComponent,RamoCreateComponent, RamoListComponent, RelatoriosComponent],
  imports: [
    CommonModule,
    PagesRoutingModule,
    LayoutModule,
    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PagesModule {
}
