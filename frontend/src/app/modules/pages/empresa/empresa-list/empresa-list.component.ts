import { Component, OnInit } from '@angular/core';
import { WebservicesService } from 'src/app/webservices.service';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import Swal from 'sweetalert2';

@Component({
  selector: 'app-empresa-list',
  templateUrl: './empresa-list.component.html',
  styleUrls: ['./empresa-list.component.scss']
})
export class EmpresaListComponent implements OnInit {

  constructor(private fb: FormBuilder, private ws: WebservicesService) { }

  formulario: FormGroup;
  empresas: any;
  edit: Boolean = false;
  todosInteresse = [];
  areaInteresse: any = [
    { valor: 'SP', name: 'São Paulo' },
    { valor: 'MG', name: 'Minas Gerais' },
    { valor: 'RJ', name: 'Rio de Janeiro' }
  ];
  categorias;
  subCategorias;
  interesse = '';
  flag = [];
  dadosFormulario;
  pesquisa = '';
  tipoPesquisa;
  dadosPesquisa: FormGroup;

  ngOnInit(): void {
    this.dadosFormulario = '';
    this.criarFormulario();
    this.ws.getEmpresa('', this.dadosPesquisa).subscribe((resposta: any) => {
      this.empresas = resposta;
    });
    this.ws.getCategoria().subscribe((resposta: any) => {
      this.categorias = resposta;
    });

  }


  onPesquisa(dado) {
    this.dadosPesquisa = new FormGroup({
      pesquisa: new FormControl(dado, [Validators.required])
    });
    this.ws.getEmpresa(this.tipoPesquisa, this.dadosPesquisa.value).subscribe((result: any) => {
      this.empresas = result;
    })
  }

  pesquisaTipo(tipo) {
    if (tipo != '') {
      this.tipoPesquisa = tipo;
    }
  }

  excluir(empresa) {

    let result = this.ws.deleteEmpresa(empresa.cnpjEmpresa);
    if (result) {
      Swal.fire({
        title: 'Sucesso',
        text: 'Empresa desativada do sistema !',
        icon: 'success',
        showCancelButton: false,
      }).then((result) => {
        if (result.value) this.load()
      })
    }
  }

  editar(empresa) {
    this.dadosFormulario = empresa;
    this.edit = true;
  }

  onCheckboxChange(e, valor) {
    if (this.formulario.value.typeCheckBox) {
      this.todosInteresse.push(valor)
    } else {
      let index = this.todosInteresse.indexOf(valor);
      this.todosInteresse.splice(index, 1);
    }
  }

  onChangeEscolha(categoria) {
    this.ws.getSubcategoria(categoria).subscribe((resposta: any) => {
      this.subCategorias = resposta;
    });
  }

  criarFormulario() {

    this.formulario = new FormGroup({
      cnpjEmpresa: new FormControl('', [Validators.required]),
      senhaAcesso: new FormControl('', [Validators.required]),
      nomeContato: new FormControl('', [Validators.required]),
      email: new FormControl('', [Validators.required]),
      telefone: new FormControl('', [Validators.required]),
      nomeEmpresa: new FormControl('', [Validators.required]),
      endereco: new FormControl('', [Validators.required]),
      categoriaAtividade: new FormControl('', [Validators.required]),
      subcategoriaAtividade: new FormControl('', [Validators.required]),
      areaInteresse: new FormControl('', [Validators.required]),
      tipoPlano: new FormControl('', [Validators.required]),
      typeCheckBox: new FormControl(null,[Validators.required])
    });
    //  this.formulario = this.fb.group({
    //   cnpjEmpresa: new FormControl(this.dadosFormulario.cnpjEmpresa, [Validators.required]),
    //   senhaAcesso: new FormControl(this.dadosFormulario.senhaAcesso, [Validators.required]),
    //   nomeContato: new FormControl(this.dadosFormulario.nomeContato, [Validators.required]),
    //   email: new FormControl(this.dadosFormulario.email, [Validators.required]),
    //   telefone: new FormControl(this.dadosFormulario.telefone, [Validators.required]),
    //   nomeEmpresa: new FormControl(this.dadosFormulario.nomeEmpresa, [Validators.required]),
    //   endereco: new FormControl(this.dadosFormulario.endereco, [Validators.required]),
    //   categoriaAtividade: new FormControl(this.dadosFormulario.categoriaAtividade, [Validators.required]),
    //   subcategoriaAtividade: new FormControl(this.dadosFormulario.subcategoriaAtividade, [Validators.required]),
    //   areaInteresse: new FormControl(this.dadosFormulario.areaInteresse, [Validators.required]),
    //   tipoPlano: new FormControl(this.dadosFormulario.tipoPlano, [Validators.required])
    // });
  }




  submitFormulario() {

    this.todosInteresse.forEach(e => {
      this.interesse += e + ',';
    })
    let verifica = false;
    if (this.formulario.value.nomeEmpresa === '') {
      this.flag[0] = true;
      verifica = true;
    }
    if (this.formulario.value.nomeContato === '') {
      this.flag[1] = true;
      verifica = true;
    }
    if (this.formulario.value.senhaAcesso === '') {
      this.flag[2] = true;
      verifica = true;
    }
    if (this.formulario.value.telefone === '') {
      this.flag[3] = true;
      verifica = true;
    }
    if (this.formulario.value.cnpjEmpresa === '') {
      this.flag[4] = true;
      verifica = true;
    }
    if (this.formulario.value.email === '') {
      this.flag[5] = true;
      verifica = true;
    }
    if (this.formulario.value.endereco === '') {
      this.flag[6] = true;
      verifica = true;
    }
    if (this.formulario.value.categoriaAtividade === '') {
      this.flag[7] = true;
      verifica = true;
    }
    if (this.formulario.value.subcategoriaAtividade === '') {
      this.flag[8] = true;
      verifica = true;
    }
    if (this.formulario.value.areaInteresse === '') {
      this.flag[9] = true;
      verifica = true;
      this.formulario.value.areaInteresse = this.interesse.substring(0, (this.interesse.length - 1));
    }
    if (this.formulario.value.tipoPlano === '') {
      this.flag[10] = true;
      verifica = true;
    }



    if (this.formulario.value.areaInteresse != '') {
      let results = this.ws.putEmpresa(this.formulario.value.cnpjEmpresa, this.formulario.value);
      if (results) {
        Swal.fire({
          title: 'Sucesso',
          text: 'Na alteração dos dados!',
          icon: 'success',
          showCancelButton: false,
        }).then((result) => {
          if (result.value) this.load()
        })
      }
    } else {
      Swal.fire({
        title: 'Revise ',
        text: 'Dados informados !',
        icon: 'info',
        showCancelButton: false,
      })
    }


  }
  load() {
    location.reload()
  }
}
